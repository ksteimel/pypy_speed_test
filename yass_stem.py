import numpy as np
from numpy import str 
import itertools
from scipy.cluster.hierarchy import linkage, to_tree, cut_tree
import re
import pickle

def write_stemmer_output(stem_lexicon, out_file_name):
    """
    This is meant to output the stemmed output in a way that I can read
    and interpret
    """
    with open(out_file_name, 'w') as out_file:
        for key in sorted(stem_lexicon.keys()):
            line = key + " : "
            for item in stem_lexicon[key]:
                line += item + ","

            out_file.write(line + "\n")

def levenshtein(word1, word2):
    """
    This calculates the levenshtein distance between two words. 
    
    It is ripped almost directly from the wikipedia entry for levenshtein distance
    """
    if len(word1) < len(word2):
        return levenshtein(word2, word1)

    if len(word2) == 0:
        return len(word1)

    previous_row = range(len(word2) + 1)
    for i, char1 in enumerate(word1):
        current_row = [i + 1]
        for j, char2 in enumerate(word2):
            insertions = previous_row[j + 1] + 1 # j+1 instead of j since previous_row and current_row are one character longer
            deletions = current_row[j] + 1       # than s2
            substitutions = previous_row[j] + (char1 != char2)
            current_row.append(min(insertions, deletions, substitutions))
        previous_row = current_row
    
    return previous_row[-1]
  
def yass_dist_1(word1, word2):
    """
    This is distance metric 1 from Majumder 2007

    In essence, each character is penalized 1 if it does not match the 
    character at that position in the other string

    Then this penalty is weighted based upon how late the discrepancy occurs
    in the string. The later it occurs, the lower the weight is and subsequently th lower the contribution to the total distance. 
    """
    dist = 0
    word1, word2 = pad_strings(word1, word2)
    if word1 == word2:
        return dist
    else:
        for char_I in range(len(word1)):
            if word1[char_I] != word2[char_I]:
                dist += (1/(2**char_I)) * 1
    
    return dist

def yass_dist_2(word1, word2):
    """
    This is distance metric 2 from Majumder 2007

    """
    dist = 0
    word1, word2 = pad_strings(word1, word2)
    length = len(word1)
    first_break = length #set this as large as possible so that we can
    # can lower it when we are able
    word1, word2 = pad_strings(word1, word2)
    if word1 == word2:
        return dist
    else:
        for char_I in range(len(word1)):
            if word1[char_I] != word2[char_I]:
                if char_I < first_break:
                    first_break = char_I
                    
    if first_break == 0:
        return 10000 #simulating infinity with a large number 
    
    for char_I in range(first_break, length):
        dist += 1/(2**(char_I - first_break))
            
    return (1/first_break) * dist 
    #weight the resulting distance by how late the first character that was different was

def yass_dist_3(word1, word2):
    """
    This is distance metric 2 from Majumder 2007

    """
    dist = 0
    word1, word2 = pad_strings(word1, word2)
    length = len(word1)
    first_break = length #set this as large as possible so that we can
    # can lower it when we are able
    word1, word2 = pad_strings(word1, word2)
    if word1 == word2:
        return dist
    else:
        for char_I in range(len(word1)):
            if word1[char_I] != word2[char_I]:
                if char_I < first_break:
                    first_break = char_I
                    
    if first_break == 0:
        return 10000 #simulating infinity with a large number 
    
    for char_I in range(first_break, length):
        dist += 1/(2**(char_I - first_break))
        
    weight = (length - first_break)/first_break
    return weight * dist 
    #weight the resulting distance by how late the first character that was different was

def yass_dist_4(word1, word2):
    """
    This is distance metric 2 from Majumder 2007

    """
    dist = 0
    word1, word2 = pad_strings(word1, word2)
    length = len(word1)
    first_break = length #set this as large as possible so that we can
    # can lower it when we are able
    word1, word2 = pad_strings(word1, word2)
    if word1 == word2:
        return dist
    else:
        for char_I in range(len(word1)):
            if word1[char_I] != word2[char_I]:
                if char_I < first_break:
                    first_break = char_I
                    
    if first_break == 0:
        return 10000 #simulating infinity with a large number 
    
    for char_I in range(first_break, length):
        dist += 1/(2**(char_I - first_break))
        
    weight = (length - first_break)/length
    return weight * dist 
    #weight the resulting distance by how late the first character that was different was

def pad_strings(word1, word2, pad_char=" "):
    """
    This function takes two strings and appends padding characters to the shorter
    string until they equal the same length

    Then both strings are returned. 

    Make sure to unpack the tuple after calling the function. 
    E.g. there are two arguments returned so be sure to have two variables to
    drop the results into
    """
    word1_len = len(word1)
    word2_len = len(word2)
    if word1_len > word2_len:
        word2 = str.ljust(word2, word1_len, pad_char)
        #word2 = word2 + (pad_char * (word1_len - word2_len))
    else:
        word1 = str.ljust(word1, word2_len, pad_char)
        #word1 = word1 + (pad_char * (word2_len - word1_len))
    
    return word1, word2

def tokenize(text):
    """
    Simple tokenization method that removes punctuation and then splits on white space
    
    Right now everything is lowercased since social media data has very inconsistent capitalization
    I don't know that things like names are necessarily more likely to be capitalized
    since people would normally use handles.
    """

    #punct = [".",",",")","(","~","+",":",'"',"'","=","@","]","[", "?","!"]
    url = "(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+"
    text2 = re.sub(url, "", text)
    punct = "[/\-.,\)\(~+:\"'=\]\[?!#%…]"
    mod_text = re.sub(punct, " ", text2)
    tokens = mod_text.split()
    #lowercasing all tokens at the moment
    tokens = [token.lower() for token in tokens]
    tokens = [token for token in tokens if token[0] not in [0,1,2,3,4,5,6,7,8,9]]
    tokens = [token for token in tokens if token[0] != '@']
    tokens = list(set(tokens))
    return tokens

def group_by_shortest(cluster, cluster_depth, word_array):
    """
    This grabs the shortest string in the cluster at the specified cluster depth
    """
    if len(word_array) != cluster.shape[0]:
        print("Incompatible cut cluster object given lenght of word array")
        print("Length of word array " + str(len(word_array)))
        print("Dimensions of cluster " + str(cluster.shape))
        
    else: 
        group_ids = cluster[:, cluster_depth]
        identity_dict = {}
        shortest_example_dict = {}
        for index in range(len(word_array)): #Create dictionary where key is group id and val is an array of word memebrs
            try:
                identity_dict[group_ids[index]].append(word_array[index])
            except KeyError:
                identity_dict[group_ids[index]] = [word_array[index]]
                
        for index in identity_dict.keys(): #Change key to be the shortest member in the group
            shortest = identity_dict[index][0]
            shortest_len = len(shortest)
            for word in identity_dict[index]:
                if len(word) < shortest_len:
                    shortest = word
                    shortest_len = len(word)
                    
            shortest_example_dict[shortest] = identity_dict[index]
        
        return shortest_example_dict
      
#def group_by_centroid(tree, cluster_depth, word_array):
        
def build_tree(word_array, dist_metric):
    """
    generate the cluster matrix for the words in the word_array. 
    """
    combos = itertools.product(word_array, word_array)
    print("Building distance matrix...")
    distances = [dist_metric(word1, word2) for word1, word2 in combos] 
    #Get all distances between each possible pair of words
    distances = np.array(distances)
    distances = distances.reshape((len(word_array), len(word_array))) #Reshape distances to be square
    condensed_distances = distances[np.triu_indices(distances.shape[0], 1)]
    #Condensed form has only the upper right triangle, the rest is 0's, needed to get the clustering to work
    print("Creating linkage...")
    linkage_matrix = linkage(condensed_distances, method = 'complete')
    #Complete method determines if clusters should be formed based upon the farthest distances between elements in the cluster
    relationship_matrix = cut_tree(linkage_matrix)
    tree = to_tree(linkage_matrix)
    return tree, relationship_matrix, distances
  
def read_deu(filename):
    """
    Read file in the germeval format
    """
    text = ""
    with open(filename) as fp:
        lines = fp.readlines()
        lines = [line.split('\t')[0] for line in lines]
        text = " ".join(lines)
        return text
      
def read_eng(filename):
    """
    read in the english file (each tweet is a line)
    """
    fp = open(filename)
    text = fp.read()
    return text
  
def main():
    filename = 'germeval2018trainorig.txt'
    input_text = read_deu(filename)
    #filename = '../data_set/sklearn/bow/train_fixed'
    #input_text = read_eng(filename)
    words = tokenize(input_text)
    dist_metric = yass_dist_2 #levenshtein
    dist_name = "yass_d2"
    lang = "deu" #"eng"
    tree, relationship_matrix, distance_matrix = build_tree(words, dist_metric)
    for cut in [1,2,3,4,5]:
        cut_point = cut * relationship_matrix.shape[0]//10
        print("Cutting at level " + str(cut_point))
        stemmed = group_by_shortest(relationship_matrix, cut_point, words)
        write_stemmer_output(stemmed, "stemmed_lexicon_" + str(cut * 10) + "_percent_" + dist_name + ".txt")
  
    dump_file = open(dist_name + lang + ".pkl","wb")
    pickle.dump({'tree':tree,
                'relationships':relationship_matrix, 
                'distance_matrix':distance_matrix,
                'words':words}, dump_file)

if __name__ == '__main__':
    main()
